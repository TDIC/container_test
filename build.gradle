import com.guidewire.gradle.helpers.ClasspathHelper
import com.guidewire.gradle.ci.CiUtil

tasks.runSuite.configure {
  appCode = project.parent.app.appCode
  generateClasspathFiles = { return ClasspathHelper.generateStartJarAndTxtFileIfRequired(project, it) }
}

/////////////////////////////////////
// schema module for: genSchemaJar //
/////////////////////////////////////
//dependencies {
//  task (':modules:schemas:genSchemaJar')
//  compile files('../schemas/gw-xml-schemas.jar')
//  schemajars files('../schemas/gw-xml-schemas.jar')
//}
///////////////////


// All memory settings are controlled from gradle.properties file
// Add custom project specific configuration here if needed

// By default run compile before running almost every tool (except few)
def tasksNotDependentOnCompile = ['flattenConfiguration', 'genExternalEntitySources', 'jsonSchemaCodegen', 'stopServer']
tasks.withType(com.guidewire.gradle.tasks.InternalToolExec) {
  if (!tasksNotDependentOnCompile.contains(it.name)) {
    dependsOn ':compile'
    doLast {
      if (gradle.taskGraph.hasTask(':compile')) {
        println """
*******************************************
A full compile was done before running the $it.name tool. To skip compile use \"-x compile\" option when running gwb
*******************************************
"""
      }
    }
  }
}

// The build can be configured to extend certain workflows. Here are some examples that we have provided
// for reference. There might be changes required based on your specific needs and requires additional testing.

// Example 1: change memory settings of dropDb tool
/*
dropDb.maxHeapSize = '4096m'
*/

// Example 2: add Git branch name and SHA to WAR name; change destination directory
/*
def branchName = 'git rev-parse --abbrev-ref HEAD'.execute().text.trim()
def sha = 'git rev-parse --short HEAD'.execute().text.trim()

// sometimes reconfiguration has to be inside afterEvaluate to override default settings
afterEvaluate {
  warJbossDbcp.destinationDir = rootProject.file('dist')
  warJbossDbcp.archiveName = "${rootProject.app.appCode}_${branchName}_${sha}_dbcp.war"

  warJbossJndi.destinationDir = rootProject.file('dist')
  warJbossJndi.archiveName = "${rootProject.app.appCode}_${branchName}_${sha}_jndi.war"
}
*/

// Example 3: add additional resources to META-INF directory in WebSphere EAR
/*
earWebsphereDbcp {
  from file('additional-descriptors-for-ear'), {
    into 'META-INF'
    include '*.*'
  }
}
*/

// Example 4: add additional JAR to Java classpath and WEB-INF/lib in WARs
/*
dependencies {
  compile files(rootProject.file('my-libs/library.jar'))
}
*/

// WEB-INF/classes is a special directory in WAR that contains classes and resources which override
// same files in JARs in WEB-INF/lib

// Example 5: copy contents of modules/configuration/webInfClasses to WEB-INF/classes in WAR
/*
tasks.withType(Jar) {
  if (it.name.startsWith('war')) {
    it.into ('WEB-INF/classes') {
      from project.file('webInfClasses')
    }
  }
}
*/
apply from: "./properties.gradle"
	
task initCodeNarc {
    doLast {
        ant.taskdef(name: 'codenarc', classname: 'org.codenarc.ant.CodeNarcTask') {
            classpath {
                fileset(dir: "${CODENARC_ROOT_DIR}/lib", includes: "*.jar")
            }
        }
    }
}

task createDeltaTmpFolder {
    doLast{
        def deltaTmpFolder = new File(CODENARC_ROOT_DIR, "deltaTmp")
        def deltaTmpFolderExists = deltaTmpFolder.exists()
        if(deltaTmpFolderExists) {
            deltaTmpFolder.deleteDir()
        }
        if(deltaTmpFolder.mkdirs()) {
            println("Succeeded creating delta tmp folder!")
        } else {
            println("Failed to create delta tmp folder!")
        }
	}
}
	
task setSystemProperties {
	System.setProperty("outputFile", CODENARC_ZIP_OUTPUT_FILE_PATH.replaceAll(" ","%20"))
	System.setProperty("exclude", "build;deploy;generated;generated_classes;plugins;xsd")
}


task unzipDelta(type: Copy) {
	def zipFile = file(CODENARC_ZIP_OUTPUT_FILE_PATH)
    def outputDir = file(CODENARC_ZIP_UNPACKED_PATH)
 
    from zipTree(zipFile)
    into outputDir
}

task deleteZippedDeltaFolder(type: Delete) {
    delete "${CODENARC_ZIP_OUTPUT_FILE_PATH}"
}

task makeReportDirectory {
    doLast{
        def reportFolder = new File(CODENARC_ROOT_DIR, "reports")
        if (!reportFolder.exists()) {
            if(reportFolder.mkdirs()) {
            println("Succeeded creating reports folder!")
            } else {
                println("Failed to create reports folder!")
            }
        }
	}
}

task codeNarcTask {
	doLast{
		def basedir = '.'
		ant.echo(message:'Beginning CodeNarc report generation...')
		ant.codenarc(rulesetfiles: file("${CODENARC_RULESET_FILE_FULL_PATH}").toURI().toString()) {
			report(type: 'xml', tofile: "${CODENARC_ROOT_DIR}/reports/${CODENARC_BASE_PRODUCT}-${CODENARC_DEFAULT_REPORT_NAME}.xml", title: 'Gosu code analysis report')
			report(type: 'html', tofile: "${CODENARC_ROOT_DIR}/reports/${CODENARC_BASE_PRODUCT}-${CODENARC_DEFAULT_REPORT_NAME}.html", title: 'Gosu code analysis report')
			report(type: 'text', tofile: "${CODENARC_ROOT_DIR}/reports/${CODENARC_BASE_PRODUCT}-${CODENARC_DEFAULT_REPORT_NAME}.txt", title: 'Gosu code analysis report')
			fileset(dir: "${CODENARC_ROOT_DIR}/deltaTmp/unpacked") {
				date(datetime: "${CODENARC_BASELINE_DATE}", pattern: 'yyyy.MM.dd hh:mm', when: 'after')
				include(name: "**/*.gs")
				include(name: "**/*.gsx")
				include(name: "**/*.gr")
				include(name: "**/*.grs")
				include(name: "**/*.pcf")
			}
		}
		ant.echo(message:'CodeNarc report generation finished!')
	}
}

task codeNarcTaskPCF {
	doLast{
		def basedir = '.'
		ant.echo(message:'Beginning PCF CodeNarc report generation...')
		ant.codenarc(rulesetfiles: file("${CODENARC_RULESET_FILE_FULL_PATH}").toURI().toString()) {
			report(type: 'html', tofile: "${CODENARC_ROOT_DIR}/reports/${CODENARC_BASE_PRODUCT}-${CODENARC_DEFAULT_REPORT_NAME}-PCF.html", title: 'Gosu code analysis report')
			fileset(dir: "${CODENARC_ROOT_DIR}/deltaTmp/unpacked") {
				date(datetime: "${CODENARC_BASELINE_DATE}", pattern: 'yyyy.MM.dd hh:mm', when: 'after')
				include(name: "**/*.pcf")
			}
		}
		ant.echo(message:'CodeNarc report generation finished!')
	}
}
task codeNarcTaskRules {
	doLast{
		def basedir = '.'
		ant.echo(message:'Beginning Rules CodeNarc report generation...')
		ant.codenarc(rulesetfiles: file("${CODENARC_RULESET_FILE_FULL_PATH}").toURI().toString()) {
			report(type: 'html', tofile: "${CODENARC_ROOT_DIR}/reports/${CODENARC_BASE_PRODUCT}-${CODENARC_DEFAULT_REPORT_NAME}-Rules.html", title: 'Gosu code analysis report')
			fileset(dir: "${CODENARC_ROOT_DIR}/deltaTmp/unpacked") {
				date(datetime: "${CODENARC_BASELINE_DATE}", pattern: 'yyyy.MM.dd hh:mm', when: 'after')
				include(name: "**/*.gr")
				include(name: "**/*.grs")
			}
		}
		ant.echo(message:'CodeNarc report generation finished!')
	}
}
task codeNarcTaskGosu {
	doLast{
		def basedir = '.'
		ant.echo(message:'Beginning GOSU CodeNarc report generation...')
		ant.codenarc(rulesetfiles: file("${CODENARC_RULESET_FILE_FULL_PATH}").toURI().toString()) {
			report(type: 'html', tofile: "${CODENARC_ROOT_DIR}/reports/${CODENARC_BASE_PRODUCT}-${CODENARC_DEFAULT_REPORT_NAME}-GOSU.html", title: 'Gosu code analysis report')
			fileset(dir: "${CODENARC_ROOT_DIR}/deltaTmp/unpacked") {
				date(datetime: "${CODENARC_BASELINE_DATE}", pattern: 'yyyy.MM.dd hh:mm', when: 'after')
				include(name: "**/*.gs")
				include(name: "**/*.gsx")
			}
		}
		ant.echo(message:'CodeNarc report generation finished!')
	}
}

task runReportAdjuster (type: JavaExec) {
	main = "csv.CSVGeneratorMain"
	classpath = files("${CODENARC_ROOT_DIR}/lib/ReportPostAdjuster.jar")
	args = ["${CODENARC_BASE_PRODUCT}","${CODENARC_ROOT_DIR}"]
}

task runCodeNarcV9 {
    dependsOn 'initCodeNarc'
    dependsOn 'createDeltaTmpFolder'
	dependsOn 'setSystemProperties'
	dependsOn 'zipChangedConfig'
	dependsOn 'unzipDelta'
	dependsOn 'deleteZippedDeltaFolder'
	dependsOn 'makeReportDirectory'
	//dependsOn 'codeNarcTask'
	dependsOn 'codeNarcTaskPCF'
	dependsOn 'codeNarcTaskRules'
	dependsOn 'codeNarcTaskGosu'
    //dependsOn 'runReportAdjuster'

	tasks.findByName('setSystemProperties').mustRunAfter 'initCodeNarc'
	tasks.findByName('setSystemProperties').mustRunAfter 'createDeltaTmpFolder'
	tasks.findByName('zipChangedConfig').mustRunAfter 'setSystemProperties'
	tasks.findByName('unzipDelta').mustRunAfter 'zipChangedConfig'
	tasks.findByName('deleteZippedDeltaFolder').mustRunAfter 'unzipDelta'
	tasks.findByName('makeReportDirectory').mustRunAfter 'deleteZippedDeltaFolder'
	//tasks.findByName('codeNarcTask').mustRunAfter 'makeReportDirectory'
    tasks.findByName('codeNarcTaskPCF').mustRunAfter 'makeReportDirectory'
    tasks.findByName('codeNarcTaskRules').mustRunAfter 'codeNarcTaskPCF'
    tasks.findByName('codeNarcTaskGosu').mustRunAfter 'codeNarcTaskRules'
	//tasks.findByName('runReportAdjuster').mustRunAfter 'codeNarcTask'
}