package com.tdic.plugin.auth

uses gw.servlet.Servlet

uses javax.servlet.http.HttpServlet
uses javax.servlet.http.HttpServletRequest
uses javax.servlet.http.HttpServletResponse

/**
 * US684, DE107
 * 02/18/2015 Rob Kelly
 *
 * A servlet to forward to an xCenter Entry Point or to the xCenter Start page if no Entry point is specified.
 *
 * When this servlet is accessed, the UserPrincipal will already have been identified by the servlet filter TDIC_SpnegoHttpFilter defined in web.xml.
 *
 * This servlet is available at: http://{servername}:{port_num}/xc/service/SSOLogin/ where xc is one of cc, bc, pc or ab.
 */
@Servlet( \ path : String -> path.matches("/SSOLogin(/.*)?"))
class TDIC_SSOLogin extends HttpServlet {

  /**
   * Handles a GET request.
   *
   * If the request is part of an existing session this method invalidates the session and redirects to itself to start a new session.
   * If the request is part of a new session this method forwards to the xCenter Entry Point specified in the request path or to the xCenter Start page if no Entry point is specified.
   */
  @Param("req", "the request the client has made of the servlet")
  @Param("resp", "the response the servlet sends to the client ")
  override function doGet(req : HttpServletRequest, resp : HttpServletResponse) {

    if (isFromExistingSession(req)) {

      req.Session.invalidate()
      resp.sendRedirect(constructPath(req.RequestURI, req.QueryString))
    }
    else {

      var forwardPath = constructPath(getEntryPointPath(req.PathInfo), req.QueryString)
      if (forwardPath == null) {

        forwardPath = constructPath("/Start.do", req.QueryString)
      }

      var context = getServletContext()
      var dispatcher = context.getRequestDispatcher(forwardPath)
      dispatcher.forward(req, resp)
    }
  }

  /**
   * Returns true if there is a JSESSIONID cookie with value matching the session id of the specified request; false otherwise.
   */
  private function isFromExistingSession(pReq : HttpServletRequest) : boolean {

    for (aCookie in pReq.Cookies) {

      if (aCookie.Name.startsWith("JSESSIONID") and aCookie.Value == pReq.Session.Id) {

        return true
      }
    }

    return false
  }

  /**
   * Constructs a path from the specified URI and query string.
   */
  private function constructPath(URI : String, queryString: String) : String {

    if (URI == null) {

      return null
    }

    var path = URI
    if (queryString != null) {

      path += "?" + queryString
    }
    return path
  }

  /**
   * Returns the entry point element(s) of the specified path info.; null if no entry point element is specified.
   */
  private function getEntryPointPath(pathInfo : String) : String {

    var pathElements = pathInfo.split("/SSOLogin")
    if (pathElements.length == 2) {

      return pathElements[1]
    }
    return null
  }
}