package com.tdic.plugin.auth

uses gw.plugin.security.AuthenticationSource

/**
 * US684
 * 01/12/2015 Rob Kelly
 *
 * An AuthenticationSource storing a username only.
 */
class TDIC_UserNameAuthenticationSource implements AuthenticationSource {

  private var username : String as UserName

  construct(aUserName : String) {

    username = aUserName
  }

  override property get Hash() : char[] {

    return null // not required
  }

  override function determineSourceComplete() : boolean {

    return username != null
  }
}