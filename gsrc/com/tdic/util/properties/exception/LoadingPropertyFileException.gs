package com.tdic.util.properties.exception

uses java.lang.Exception
uses java.lang.StringBuilder
uses java.lang.Throwable

/**
 * Created by Souvik Kar on 5/13/2019.
 */
class LoadingPropertyFileException extends Exception {
  /**
   * Instantiates a new loading property file exception.
   *
   * @param propertyFile the property file
   */
  construct(propertyFile: String) {
    super(createMessage(propertyFile, "Unable to load properties file"))
  }

  /**
   * Instantiates a new loading property file exception.
   *
   * @param propertyFile the property file
   * @param message the message
   */
  construct(propertyFile: String, message: String) {
    super(createMessage(propertyFile, message))
  }

  /**
   * Instantiates a new property not found exception.
   *
   * @param propertyFile the property file
   * @param cause the cause
   */
  construct(propertyFile: String, cause: Throwable) {
    super(createMessage(propertyFile, "Unable to load properties file"), cause)
  }

  /**
   * Instantiates a new loading property file exception.
   *
   * @param propertyFile the property file
   * @param message the message
   * @param cause the cause
   */
  construct(propertyFile: String, message: String, cause: Throwable) {
    super(createMessage(propertyFile, message), cause);
  }

  private static function createMessage(propertyFile: String, message: String): String {
    var msg = new StringBuilder(message)
    msg.append(". Property file: ")
    msg.append(propertyFile)
    return msg.toString()
  }
}