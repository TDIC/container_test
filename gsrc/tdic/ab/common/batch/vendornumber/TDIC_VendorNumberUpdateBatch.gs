package tdic.ab.common.batch.vendornumber

uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.Query
uses gw.pl.exception.GWConfigurationException
uses gw.pl.logging.LoggerCategory
uses gw.processes.BatchProcessBase
uses tdic.util.cache.CacheManager
uses tdic.util.cache.DatabaseParam
uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.misc.email.EmailUtil
uses tdic.ab.common.batch.vendornumber.dto.TDIC_VendorRecord

uses java.io.IOException
uses java.lang.Exception
uses java.lang.StringBuffer
uses java.sql.Connection
uses java.sql.SQLException
uses java.util.ArrayList
uses java.util.HashMap
uses java.util.Map
uses java.sql.ResultSet

/**
 * US556
 * 04/08/2015 Kesava Tavva
 *
 * Implementation of custom batch process for updating vendor number of contact. Checks initial conditions to run batch process,
 * retrieving Contact records which don't have vendor number, reads vendor numbers from Lawson datatbase.
 */
class TDIC_VendorNumberUpdateBatch extends BatchProcessBase {

  //Sudheendra : Referred Logger for Vendor Update
  private static var _logger = LoggerCategory.TDIC_VENDORNUMUPD
  private static var CLASS_NAME = "TDIC_VendorNumberUpdateBatch"
  private var _errorRecords : List<TDIC_VendorRecord>
  private static final var BATCH_USER = "su"
  private var _emailTo : String as readonly EmailTo
  private var _failureEmailTo : String as readonly FailureEmailTo
  //private var _lawsonDBParam : DatabaseParam as readonly ExternalAppDBParam
  private var _lawsonDBParam = new DatabaseParam()
  private static final var EMAIL_TO_PROP_FIELD = "VendorNumberBatchNotificationEmail"
  private static final var FAILURE_EMAIL_RECIPIENT = "CMInfraIssueNotificationEmail"
  private static final var EXTERNAL_APP_NAME = "Lawson"
  private static final var EXTERNAL_APP_HOST = "lawson.Host"
  private static final var EXTERNAL_APP_INSTANCE = "lawson.Instance"
  private static final var EXTERNAL_APP_PORT = "lawson.Port"
  private static final var EXTERNAL_APP_DBNAME = "lawson.DbName"
  private static final var EXTERNAL_APP_USERNAME = "lawson.UserName"
  private static final var EXTERNAL_APP_PASSWORD = "lawson.Password"

  private static final var FAILURE_SUB = "Failure::${gw.api.system.server.ServerUtil.ServerId}-Vendor number Update Batch Job Failed"
  private static final var SUCCESS_SUB = "Success::${gw.api.system.server.ServerUtil.ServerId}-Vendor number Update Batch Job completed successfully"
  private static final var PARTIAL_SUCCESS_SUB = "PartialSuccess::${gw.api.system.server.ServerUtil.ServerId}-Vendor number Update Batch Job completed successfully with errors and/or warnings"

  construct(){
    super(BatchProcessType.TC_VENDORNUMBERUPDATE)
  }

  override function doWork() {
    var logPrefix = "${CLASS_NAME}#doWork()"
    _logger.debug("${logPrefix} - Entering")
    try {
      var contacts = retrieveABContacts()
      _logger.debug("${logPrefix} - Contacts found without VendorNumber are: ${contacts}")
      if(!contacts.HasElements){
        EmailUtil.sendEmail(EmailTo, SUCCESS_SUB, "No TaxIDs found without VendorNumber.")
        _logger.debug("${logPrefix} - Exiting")
        return
      }
      var taxIDsList =  getTaxIDsList(contacts)
      var vendorNumberRecords = retrieveVendorNumbers(taxIDsList)
      _logger.debug("${logPrefix} - VendorNumber records retrieved are: ${vendorNumberRecords}")

      if(vendorNumberRecords?.size() == 0){
        EmailUtil.sendEmail(EmailTo, SUCCESS_SUB, "VendorNumbers not found in Lawson for taxIDs : ${taxIDsList}")
        _logger.debug("${logPrefix} - Exiting")
        return
      }

      OperationsExpected = vendorNumberRecords.size()
      processVendorNumberUpdates(contacts, vendorNumberRecords)

      if(_errorRecords.HasElements)
        EmailUtil.sendEmail(EmailTo, PARTIAL_SUCCESS_SUB, "${buildEmailDescription()}")
      else
        EmailUtil.sendEmail(EmailTo, SUCCESS_SUB, "Vendor number update batch process successfully completed.")

      _logger.debug("${logPrefix} - Exiting")

    }catch(e : Exception){
      EmailUtil.sendEmail(FailureEmailTo, FAILURE_SUB, "Vendor number update batch process failed with errors. Please review below error details. \n ${e.StackTraceAsString}")
      _logger.error("${logPrefix} - Batch process failed with erorrs.", e)
    }
  }

  /**
   * US556
   * 04/08/2015 Kesava Tavva
   *
   * Updates vendor number with the details retrieved from Lawson system.
   */
  @Param("contacts","List of ABContact records retrtieved for vendor number updates")
  @Param("vendorNumberRecords","Vendor number records retrieved from Lawson system")
  private function processVendorNumberUpdates(contacts : List<ABContact>, vendorNumberRecords : Map<String, TDIC_VendorRecord>) {
    var logPrefix = "${CLASS_NAME}#processVendorNumberUpdates(List, Map)"
    _logger.debug("${logPrefix} - Entering")
    _errorRecords = new ArrayList<TDIC_VendorRecord>()
    for(contact in contacts){
      var vendorNumberRecord = vendorNumberRecords.get(contact.TaxID)
      if(vendorNumberRecord != null) {
        try{
           if(vendorNumberRecord.VendorNumber == null){
            vendorNumberRecord.Comments = "Vendor number is null for TaxID : ${vendorNumberRecord.TaxID}"
            _errorRecords.add(vendorNumberRecord)
            incrementOperationsFailed()
          }
          else {
            _logger.debug("${logPrefix} - vendor number update starting for '${contact.TaxID}'")
            gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
              contact = bundle.add(contact)
              contact.VendorNumber = vendorNumberRecord.VendorNumber
            }, BATCH_USER)
            _logger.debug("${logPrefix} - Vendor number update completed for '${vendorNumberRecord.TaxID}'")
            incrementOperationsCompleted()
          }
        }catch(e : Exception){
          vendorNumberRecord.Comments = e.StackTraceAsString
          _errorRecords.add(vendorNumberRecord)
          incrementOperationsFailed()
        }
      }
    }
    _logger.debug("${logPrefix} - Exiting")
  }

  /**
   * US556
   * 04/08/2015 Kesava Tavva
   *
   * Builds Email description with details from Error vendor number update records
   */
  @Returns("String, Description of error records")
  private function buildEmailDescription() : String {
    var description = new StringBuffer()
    description.append("Vendor number update batch process completed with Errors and/or Warnings.\n")
    if(_errorRecords.HasElements){
      description.append("Error Records are:-\n")
      _errorRecords.each( \ record -> {
        description.append("Error in processing vendor number update for the tax id: '${record.TaxID}'. Details are: ${record.Comments}\n")
      })
    }
    return description?.toString()
  }

  /**
   * US556
   * 04/08/2015 Kesava Tavva
   *
   * Determines if the process should run the doWork() method, based on successful retrieval of configured properties from int database.
   */
  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  @Throws(GWConfigurationException, "If there are setup issues, such as issues with retrieving properties from the integration database")
  override function checkInitialConditions() : boolean {
     var logPrefix = "${CLASS_NAME}#checkInitialConditions()"
    _logger.debug("${logPrefix} - Entering")
    _emailTo = PropertyUtil.getInstance().getProperty(EMAIL_TO_PROP_FIELD)//CacheManager.PropertiesCache.get(EMAIL_TO_PROP_FIELD)
    _failureEmailTo = PropertyUtil.getInstance().getProperty(FAILURE_EMAIL_RECIPIENT)//CacheManager.PropertiesCache.get(FAILURE_EMAIL_RECIPIENT)
    _logger.trace("${logPrefix} - ${EMAIL_TO_PROP_FIELD}-${_emailTo}")
    //_lawsonDBParam = CacheManager.AppDatabaseCache.get(EXTERNAL_APP_NAME)
    _lawsonDBParam.Host = PropertyUtil.getInstance().getProperty(EXTERNAL_APP_HOST)
    _lawsonDBParam.Instance = PropertyUtil.getInstance().getProperty(EXTERNAL_APP_INSTANCE)
    _lawsonDBParam.Port = PropertyUtil.getInstance().getProperty(EXTERNAL_APP_PORT)
    _lawsonDBParam.DbName = PropertyUtil.getInstance().getProperty(EXTERNAL_APP_DBNAME)
    _lawsonDBParam.UserName = PropertyUtil.getInstance().getProperty(EXTERNAL_APP_USERNAME)
    _lawsonDBParam.Password = PropertyUtil.getInstance().getProperty(EXTERNAL_APP_PASSWORD)

    var initFailureMsg:StringBuffer
    if(_emailTo == null){
      initFailureMsg = new StringBuffer()
      initFailureMsg.append("Failed to retrieve notification email addresses with the key '${EMAIL_TO_PROP_FIELD}' from integration database.")
    }
    if(_failureEmailTo == null){
      initFailureMsg = new StringBuffer()
      initFailureMsg.append("Failed to retrieve notification email addresses with the key '${FAILURE_EMAIL_RECIPIENT}' from integration database.")
    }
    if(_lawsonDBParam == null){
      if(initFailureMsg == null)
      initFailureMsg = new StringBuffer()
    else
      initFailureMsg.append(" | ")
      initFailureMsg.append("Failed to retrieve External App database connection details with the key '${EXTERNAL_APP_NAME}' from integration database.")
    }

    if (initFailureMsg?.toString().HasContent) {
      if(_failureEmailTo != null)
        EmailUtil.sendEmail(FailureEmailTo, FAILURE_SUB, "Unable to initiate batch process. Please review log file for more details.")
      throw new GWConfigurationException(initFailureMsg.toString())
    }
    _logger.debug("${logPrefix} - Exiting")
    return Boolean.TRUE
  }

  /**
   * US556
   * 04/08/2015 Kesava Tavva
   *
   * Retrieving ABContacts which don't have vendor number
   */
  @Returns("List, ABContact records list")
  protected function retrieveABContacts() : List<ABContact> {
    var logPrefix = "${CLASS_NAME}#retrieveABContacts()"
    _logger.debug("${logPrefix}- Entering")
    var pQuery = Query.make(ABContact).compare("Subtype", NotEquals, typekey.ABContact.TC_ABUSERCONTACT)
        .compare("Subtype", NotEquals, typekey.ABContact.TC_ABPLACE).compare("VendorNumber", Equals, null).compare("TaxID", NotEquals, null)
        .join("PrimaryAddress").compare(Address#AddressLine1, NotEquals , null).compare(Address#City, NotEquals, null)
        .compare(Address#State, NotEquals, null).compare(Address#PostalCode, NotEquals, null)
    _logger.debug("${logPrefix} - Exiting")
    return pQuery.select()?.toList()
  }

  /**
   * US556
   * 04/08/2015 Kesava Tavva
   *
   * Retrieve Check status update records from Lawson using JDBC connection.
   */
  @Returns("Map, list of Vendor number records retrieved from Lawson`1")
  @Throws(IOException, "If there are any IO errors")
  @Throws(SQLException, "If there are JDBC issues such as connectivity, reading data")
  @Throws(Exception, "If there are any other errors")
  private function retrieveVendorNumbers(taxIDsList : List<String>): Map<String,TDIC_VendorRecord> {
    var logPrefix = "${CLASS_NAME}#retrieveVendorNumbers(List<String>)"
    _logger.debug("${logPrefix}- Entering")

    var vendorNumberRecords : Map<String, TDIC_VendorRecord>
    if(!taxIDsList.HasElements){
      _logger.debug("${logPrefix} - Exiting")
      return vendorNumberRecords
    }

    var taxIDs = buildTaxIDsList(taxIDsList)
    var con: Connection
    var resultSet: ResultSet

    try {
      con = DatabaseManager.getConnection(_lawsonDBParam.Host,
          _lawsonDBParam.Instance,
          _lawsonDBParam.Port,
          _lawsonDBParam.DbName,
          _lawsonDBParam.UserName,
          _lawsonDBParam.Password)

      var sqlStmt = "SELECT TAX_ID,VENDOR FROM APVENMAST WHERE TAX_ID IN (${taxIDs}) AND VENDOR <> ''"
      _logger.debug("${logPrefix} - sqlstmt: ${sqlStmt}")
      resultSet = DatabaseManager.executeQuery(con, sqlStmt)
      vendorNumberRecords = new HashMap<String,TDIC_VendorRecord>()
      while(resultSet.next()){
        var record = new TDIC_VendorRecord()
        record.TaxID = resultSet.getString("TAX_ID").trim()
        record.VendorNumber = resultSet.getString("VENDOR")
        vendorNumberRecords.put(record.TaxID, record)
      }
       _logger.debug("${logPrefix} - Exiting")
      return vendorNumberRecords
    } catch (io:IOException) {
      _logger.error("${logPrefix} - IOException= " + io)
      EmailUtil.sendEmail(FailureEmailTo, FAILURE_SUB, "TDIC_VendorNumberUpdateBatch#retrieveVendorNumbers()- VendorNumber update batch failed due to :" + io.LocalizedMessage)
      throw(io)
    } catch (sql:SQLException) {
      _logger.error("${logPrefix} - SQLException= " + sql)
      EmailUtil.sendEmail(FailureEmailTo, FAILURE_SUB, "TDIC_VendorNumberUpdateBatch#retrieveVendorNumbers()- VendorNumber update batch failed due to :" + sql.LocalizedMessage)
      throw(sql)
    } catch (e:Exception) {
      _logger.error("${logPrefix} - Exception= " + e)
      EmailUtil.sendEmail(FailureEmailTo, FAILURE_SUB, "TDIC_VendorNumberUpdateBatch#retrieveVendorNumbers()- VendorNumber update batch failed due to :" + e.LocalizedMessage)
      throw(e)
    } finally {
      try {
        if(resultSet != null) resultSet.close()
        if (con != null) con.close()
      } catch (fe:Exception) {
         _logger.error("${logPrefix} - finally Exception= " + fe)
      }
    }
  }

  /**
  * US556
  * 04/08/2015 Kesava Tavva
  *
  * Build String object based on list of TaxIDs of ABContact records retrieved for Vendor number updates.
  */
  @Returns("String, String objecte contains taxids separated by comma")
  private function buildTaxIDsList(taxIDsList : List<String>) : String {
    var taxIDs : StringBuffer
    if(taxIDsList.HasElements){
      taxIDs = new StringBuffer()
      taxIDsList.eachWithIndex( \ id, index -> {
        if(index != taxIDsList.size()-1)
          taxIDs.append("'${id}',")
        else
          taxIDs.append("'${id}'")
      })
    }
    return taxIDs?.toString()
  }
  /**
   * US556
   * 04/08/2015 Kesava Tavva
   *
   * Extract tax ids from all contact records that are available for vendor number updates
   * and build a list of those tax ids.
   */
  @Param("paymentRecords", "List of ABContact records that are available for vendor number updates")
  @Returns("List<String>, list of tax ids from payment records")
  private function getTaxIDsList(contacts : List<ABContact>) : List<String> {
    var taxIDsList = new ArrayList<String>()
    contacts.each( \ contact -> {
      taxIDsList.add(contact.TaxID)
    })
    return taxIDsList
  }
}