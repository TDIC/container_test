package tdic.ab.config.enhancements

/**
 * US1041
 * 03/19/2015 Rob Kelly
 *
 * TDIC Person Enhancement.
 */
enhancement TDIC_ABPersonEnhancement : entity.ABPerson {

  // Amended TaxID -- SSN/FEIN feild mapping, also amended PersonFEIN usage as part of fix for GW-1623 & GW-753
  property get FEINOfficialID_TDIC() : String {
    var FEINOfficialID = this.OfficialIDs.firstWhere( \ a -> a.OfficialIDType == OfficialIDType.TC_FEIN)
    return FEINOfficialID.OfficialIDValue
  }

  property set FEINOfficialID_TDIC(FEIN : String) {
    var FEINOfficialID = this.OfficialIDs.firstWhere( \ id -> id.OfficialIDType == typekey.OfficialIDType.TC_FEIN)
    if (FEINOfficialID == null) {
      FEINOfficialID = new ABOfficialID()
      FEINOfficialID.OfficialIDType = typekey.OfficialIDType.TC_FEIN
      this.addToOfficialIDs(FEINOfficialID)
    }
    FEINOfficialID.OfficialIDValue = FEIN
  }
}
