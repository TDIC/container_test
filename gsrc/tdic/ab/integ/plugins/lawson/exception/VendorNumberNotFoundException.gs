package tdic.ab.integ.plugins.lawson.exception

uses java.lang.Exception

class VendorNumberNotFoundException extends Exception {
  // Default Constructor

  construct() {
  }

  construct(aMessage : String) {
    super(aMessage)
  }
}