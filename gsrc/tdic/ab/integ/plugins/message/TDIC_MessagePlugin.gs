package tdic.ab.integ.plugins.message

uses com.tdic.util.misc.email.EmailUtil
uses tdic.util.cache.CacheManager
uses gw.pl.logging.LoggerCategory
uses gw.plugin.messaging.MessageTransport

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 5/7/15
 * Time: 3:26 PM
 * To change this template use File | Settings | File Templates.
 */
abstract class TDIC_MessagePlugin implements MessageTransport {

  private var _logger = LoggerCategory.TDIC_INTEGRATION
  protected var _destId : int = 0

  property set DestinationID(aDestId: int): void {
    _destId = aDestId
  }

  override function suspend() {
    var _emailRecipient = CacheManager.PropertiesCache.get("InfraIssueNotificationEmail")
    if(_emailRecipient == null){
      _logger.error("TDIC_WCpolsTransport#suspend() - Cannot retrieve notification email addresses with the key 'InfraIssueNotificationEmail' from integration database.")
    }
    else
      EmailUtil.sendEmail(_emailRecipient, "Destination suspended in :" + gw.api.system.server.ServerUtil.Product.ProductName + " at " + gw.api.system.server.ServerUtil.ServerId, "Destination with ID: " + _destId + " is suspended.")
  }

}