package tdic.ab.integ.plugins.processing

uses gw.pl.logging.LoggerCategory
uses gw.plugin.processing.IProcessesPlugin
uses gw.processes.BatchProcess
uses tdic.ab.common.batch.vendornumber.TDIC_VendorNumberUpdateBatch
uses tdic.util.cache.CacheManagerBatchProcess

@Export
class TDICProcessesPlugin implements IProcessesPlugin {


  private static final var _logger = LoggerCategory.TDIC_INTEGRATION

  override function createBatchProcess(type : BatchProcessType, arguments : Object[]) : BatchProcess {
    switch(type) {
      case BatchProcessType.TC_CACHEMANAGER:
          return new CacheManagerBatchProcess()
      //US556 - Vendor number updates
      case BatchProcessType.TC_VENDORNUMBERUPDATE:
          return new TDIC_VendorNumberUpdateBatch()
      default:
        return null
    }
  }
}