package tdic.ab.integ.services.lawson.dto

uses gw.xml.ws.annotation.WsiExportable

/**
 * Sprint - 5, GINTEG - 21 : CM Lawson Contact API
 * Below DTO will contain Old Vendor Numbers for which there was no match in CM
 * Author: Sudheendra
 * Date: 09/14/2019
 */
@WsiExportable
final class TDIC_OldVendorNotFoundDTO {

  var _old_vendor : String as OLD_VENDOR

}