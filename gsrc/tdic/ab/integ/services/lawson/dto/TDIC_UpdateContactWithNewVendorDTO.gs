package tdic.ab.integ.services.lawson.dto

uses gw.xml.ws.annotation.WsiExportable

/**
 * Sprint - 5, GINTEG - 21 : CM Lawson Contact API
 * Below DTO will contain 'OLD Vendor' and 'New Vendor' details for the newly created contacts
 * Author: Sudheendra
 * Date: 09/14/2019
 */
@WsiExportable
final class TDIC_UpdateContactWithNewVendorDTO {

  var _new_vendor : String as NEW_VENDOR

  var _old_vendor : String as OLD_VENDOR

}